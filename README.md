# Website Bits & Bäume Berlin

This repository stores the raw content of https://berlin.bits-und-baeume.org


## How to edit this website

### Prepare
- Clone this repository
- [Install hugo](https://gohugo.io/getting-started/installing/)
- Inside the cloned repository, run `hugo server -w`
- You should see the website at `localhost:1313`

### Perform changes
- Now change the content of the website: First checkout a new branch (master branch is write-protected) with `git checkout -B <choose a descriptive name of your edit>`
- Perform your edits and check the local hugo server how your changes look like.
- If you are happy, git add, commit and push your changes to the new branch (the command line will help you with the correct commands)


## Technical Notes

The website is not only available at https://berlin.bits-und-baeume.org but
also at GitLab's internal URL https://bub-berlin.gitlab.io/website/

### CSS

We are using CSS that is derived from the CSS of https://bits-und-baeume.org
and has only some small modification. The main change is the additional file
`assets/css/_column.sass`. You can compare the `assets/css` folder against the
[sass folder](https://github.com/AfeefaDe/bitbaum/tree/master/sass) of the
[bitbaum repository](https://github.com/AfeefaDe/bitbaum). Our copy was created
form version
[962548](https://github.com/AfeefaDe/bitbaum/tree/9625482e94ee89629e6400bc6cf7e4be08270ccf)
of the repository.