---
title: "Bits & Bäume Berlin"
baseUrl: Site.baseUrl
---

Lokales Netzwerk für
# Digitalisierung und Nachhaltigkeit

**Kontakt:**

* E-Mail: berlin{ät}bits-und-baeume.org
* [Forum](https://discourse.bits-und-baeume.org)
* [Mastodon](https://mastodon.bits-und-baeume.org)
* [Twitter](https://twitter.com/bitsundbaeume_B)
* [Peertube](https://tube.tchncs.de/accounts/bits_und_baeume_berlin/video-channels)

</br>

Digitalisierung und Nachhaltigkeit sind die entscheidenden, existenziellen Herausforderungen
des 21. Jahrhunderts, werden bislang aber kaum gemeinsam gedacht oder diskutiert. Dabei liegen die Fragen auf der Hand: </br></br>

* Wie kann die digitale Gesellschaft demokratisch und gerecht gestaltet werden?
* Wie kann Digitalisierung darauf ausgerichtet werden, unsere Lebensgrundlagen auf diesem Planeten zu bewahren? Welche Chancen stecken in digitalen Anwendungen z.B. für Klima- und Ressourcenschutz?
* Welche Art von Digitalisierung steht diesen Zielen entgegen?

Motiviert und inspiriert durch die Konferenz [Bits & Bäume 2018](https://bits-und-baeume.org/de) haben sich ein paar Teilnehmer:innen daran gemacht, die Idee auf die lokale Ebene in Berlin zu übertragen.
