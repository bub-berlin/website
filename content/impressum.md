---
title: "Impressum"
---

### Verantwortlich und Inhalt / Gestaltung und Konzeption:

- Verein zur Förderung einer nachhaltigen urbanen Kultur (VFnuK e.V.)
- c/o Max Martens, Gerichtstr. 23, 13347 Berlin
- Telefon: +49 176 78566593
- E-Mail: vfnuk@mailbox.org
- Gemeinschaftlich vertretungsberechtigt: Max Martens, Maria Marggraf